## {Bureaucracktic Bedtime Stories}


{Bureaucracktic Bedtime Stories} is a collection of contributions in
many media that originate from the worksession Bureaucracksy. This work
session brought together artivist practices around the imaginative
re-appropriations of rules and regulations, and took place between 7 and
12 December 2020. Bureaucracksy investigated the governance of
techno-social systems through the prism of bureaucracy. The execution of
rules is an essential element of computation, of digital
infrastructures, and of the societies that they operate with. Critical
practices of listing, naming, filtering, tool making, care and sharing
require that some books are kept.

**{How to receive this}**

Subscribe before 1 june 2022, by

**E-Mail:** you can register to the [mailing list](https://boucan.domainepublic.net/mailman3/postorius/lists/bureaucracksy.lists.constantvzw.org)  
**Podcast:** add [this link](https://bureaucracksy.constantvzw.org/podcast.xml) to your podcast feed:

After registration, you will be presented one story per day, at 20:30
CET, 21 days in a row. Once you have received all contributions, you
will have to unregister. Unless you want to enjoy another cycle!

The stories can be enjoyed as a podcast or through a mailinglist, on
your phone or your laptop. Whatever you prefer as a bedside reading
companion technology.

### {Contributions}

{Bureaucracktic Bedtime Stories} contains contributions by:
Sarrita Hunn, Loup Cellard, Richard Wheeler, Temporary Bureaucracksy
Collective, Elodie Mugrefya, Muslin Brothers, Office for Joint
Administrative Intelligence (O.J.A.I.), Lucia Palladino, Wendy Van
Wynsberghe, Laura Oriol + Peter Westenberg, Melt - Loren Britton &
Isabel Paehr, Francis Hunger, Brendan Howell, Caterina M, Simon Browne,
An Mertens, Eric Snodgrass, Jonathan McHugh, Simon Browne

### Credits

Constant 2022. {Copyleft with a difference:} This is a collective work,
you are invited to copy, distribute, and modify it under the terms of
the [CC4r](https://constantvzw.org/wefts/cc4r.en.html->https://constantvzw.org/wefts/cc4r.en.html).

**{Editing}**  
Peter Westenberg, Wendy Van Wynsberghe, Manufactura Independente

**{Design}**  
Manufactura Independente

**{Financial support}**  
Federation Wallonie Bruxelles, Vlaamse Overheid, Vlaamse
Gemeenschapscommissie


For Bureaucracksy information / documentation see:  
<https://constantvzw.org/wefts/bureaucracksy.en.html>

For info on Constant see:  
<https://constantvzw.org/>


**{Sources}**

- [Luculent](http://eastfarthing.com/luculent/) designed by Andrew Kensler and
licensed with [OFL](http://scripts.sil.org/OFL)
- [WonderType](https://www.clemencefontaine.fr/page/wonder.html) designed by
Clémence Fontaine and licensed with [OFL](http://scripts.sil.org/OFL)
- Pixel chairs and plant from [Hospital TopDown Perspective 2D Pixel
Art](https://opengameart.org/content/hospital-topdown-perspective-2d-pixel-art)
designed by [Rodrimanu360](https://opengameart.org/users/rodrimanu360) and
published under [CC-BY 4.0](https://opengameart.org/sites/default/files/license_images/cc-by_0.png).
