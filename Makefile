SSH_PATH=manufactura:~/apps/bedtime-stories-static/
SHELL=/bin/bash
ACTIVATE=. `pwd`/.env/bin/activate

build:
	mkdir -p output output/media output/img
	$(ACTIVATE); PYTHONIOENCODING=utf-8 python3 lib/generate.py
	cp -rf assets/* output/
	cp content/**/*.mp3 output/media
	cp content/**/*.jpg output/img
	cp content/**/*.png output/img

install:
	python3 -m venv .env
	. `pwd`/.env/bin/activate; pip install -r requirements.txt

serve:
	. `pwd`/.env/bin/activate; cd output && python -m http.server

clean:
	rm -fr output/

deploy: build
	rsync --compress --checksum --progress --delete --recursive --update output/ $(SSH_PATH) --exclude="__pycache__" --exclude=".*"
dry-deploy: build
	rsync --compress --checksum --progress --delete --recursive --update output/ $(SSH_PATH) --exclude="__pycache__" --exclude=".*" --dry-run
