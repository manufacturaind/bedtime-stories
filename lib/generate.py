#!/usr/bin/env python3
import jinja2
import markdown
import os
import subprocess
from pathlib import Path
from feedgen.feed import FeedGenerator

CURRENT_NUMBER = 21
BASE_URL = "https://bureaucracksy.constantvzw.org/"

TEMPLATES_DIR = "templates/"
content_path = Path("content/")
output_path = Path("output/")

script_path = os.path.dirname(os.path.realpath(__file__))

def render_template_into_file(env, templatename, filename, context=None):
    template = env.get_template(templatename)
    if not context:
        context = {}
    html = template.render(**context)
    with open(filename, "wb") as fh:
        fh.write(html.encode("utf-8"))


env = jinja2.Environment(
    loader=jinja2.FileSystemLoader([TEMPLATES_DIR]),
    trim_blocks=True,
    lstrip_blocks=True,
)


def read_markdown_file(filename):
    if not isinstance(filename, Path):
        file_path = content_path.joinpath(filename)
    else:
        file_path = filename
    md = markdown.Markdown(extensions=["full_yaml_metadata", "fenced_code", "tables", "toc"])
    content = open(str(file_path), "r").read()
    html = md.convert(content)
    result = {}
    if md.Meta:
        result.update(md.Meta)
    # each meta field value is a list (to preserve linebreaks, see
    # https://python-markdown.github.io/extensions/meta_data/ )
    # we don't want this, so extract the single items
    result["content"] = html
    result["orig"] = content
    return result


def mkdir(dirpath):
    # creates a dir if it doesn't exist
    if not os.path.exists(dirpath):
        os.makedirs(dirpath)


def generate():
    stories = []
    dirs = content_path.glob("*/")
    from pprint import pprint

    for d in sorted(dirs):
        if not os.path.isdir(str(d)):
            continue
        mdfiles = list(d.glob("*.md"))
        if not mdfiles:
            print(f"{d} has no .md files, skipping")
            continue
        filepath = mdfiles[0]
        stories.append(
            {
                "filename": filepath.name,
                "path": filepath,
                "md": read_markdown_file(filepath),
                "slug": filepath.name.replace(".md", "").replace("&", "-"),
                "number": int(d.name.split('-')[0])
            }
        )
    from pprint import pprint

    # generate pages
    for story in stories:
        target_path = output_path.joinpath(story["slug"])
        target_path.mkdir(parents=True, exist_ok=True)
        render_template_into_file(
            env,
            "page.html",
            target_path.joinpath("index.html"),
            {"story": story, "stories": stories},
        )
    # generate index
    render_template_into_file(
        env,
        "index.html",
        output_path.joinpath("index.html"),
        {"stories": stories},
    )

    # generate podcast feed
    fg = FeedGenerator()
    fg.load_extension("podcast")

    fg.title("Bureaucracktic Bedtime Stories")
    fg.author(name="Constant")
    fg.podcast.itunes_author("Constant")
    fg.language("en")
    fg.id(BASE_URL)
    fg.logo(BASE_URL + "img/icon_chair-1280px.png")
    fg.link(href=BASE_URL, rel="self")

    desc = read_markdown_file('00-Podcast-description.txt').get('content')
    fg.description(desc)

    for story in stories:
        # print( + ": "+ story.get("slug"))
        if story.get("number") > CURRENT_NUMBER:
            break
        fe = fg.add_entry()
        fe.title(story["md"].get("title") + " — " + story["md"].get("contributor"))
        # fe.description(story["md"].get("description"))
        fe.description(story["md"].get("content"))

        # fe.id("https://test.com/media.mp3")
        author = story["md"].get("contributor")
        if not author:
            author = story["md"].get("authors")
        fe.author(email=author, name=author)

        mediafiles = story["md"].get("media")
        if mediafiles:
            if type(mediafiles) == list:
                pass
            else:
                filename = story["path"].parent.joinpath(mediafiles)
                duration = subprocess.run(
                    'mp3info -p "%S\n" ' + str(filename),
                    shell=True,
                    stdout=subprocess.PIPE,
                ).stdout.strip()
                fe.enclosure(BASE_URL + "media/" + mediafiles, duration, "audio/mpeg")
        else:
            filename = 'silence-10s.mp3'
            fe.enclosure(BASE_URL + filename, "10", "audio/mpeg")

        # pprint(story["path"].parent)
    fg.rss_str(pretty=True)
    fg.rss_file("output/podcast.xml")


if __name__ == "__main__":
    generate()
