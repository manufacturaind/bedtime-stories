---
title: Open-Source Borders
contributor: Richard Wheeler
type: link, text, image
---

In Open Source Borders for “Bureaucracksy Bed Time Stories”, I invite you to watch clips from some of these programs and answer questions about them. This may be the first time that you've had an opportunity to look at parts of the border apparatus not as a participant being controlled as you move through it, but instead as an observer. What do you see?

---

[richardwheeler.com/open-source-borders/](http://www.richardwheeler.com/open-source-borders/)

![Slide 1](https://bedtime.manufacturaindependente.org/img/open-source-borders-slide-1.png)  
![Slide 2](https://bedtime.manufacturaindependente.org/img/open-source-borders-slide-2.png)  
![Slide 3](https://bedtime.manufacturaindependente.org/img/open-source-borders-slide-3.png)  
![Slide 4](https://bedtime.manufacturaindependente.org/img/open-source-borders-slide-4.png)  
![Slide 5](https://bedtime.manufacturaindependente.org/img/open-source-borders-slide-5.png)  
![Slide 6](https://bedtime.manufacturaindependente.org/img/open-source-borders-slide-6.png)  
![Slide 7](https://bedtime.manufacturaindependente.org/img/open-source-borders-slide-7.png)  
![Slide 9](https://bedtime.manufacturaindependente.org/img/open-source-borders-slide-8.png)  
![Slide 10](https://bedtime.manufacturaindependente.org/img/open-source-borders-slide-9.png)
