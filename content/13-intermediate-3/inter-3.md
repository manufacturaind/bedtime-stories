---
title: Participatory music
contributor: Temporary Bureaucracksy Collective
media: output.mp3
---

A sound edit of deliberate sound contributions to online breaks
improvised together in the Bureaucracksy digital environment.
