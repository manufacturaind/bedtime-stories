---
title: Pageants and Pleasures
contributor: Brendan Howell, Caterina M, Simon Browne
type: image
pages: 14
---

Warming up to Bureaucracy;  
A choreography to find pleasure in procedure.

---

![Slide  1](https://bedtime.manufacturaindependente.org/img/pageants-and-pleasures-slide-1.png)
![Slide  2](https://bedtime.manufacturaindependente.org/img/pageants-and-pleasures-slide-2.png)
![Slide  3](https://bedtime.manufacturaindependente.org/img/pageants-and-pleasures-slide-3.png)
![Slide  4](https://bedtime.manufacturaindependente.org/img/pageants-and-pleasures-slide-4.png)
![Slide  5](https://bedtime.manufacturaindependente.org/img/pageants-and-pleasures-slide-5.png)
![Slide  6](https://bedtime.manufacturaindependente.org/img/pageants-and-pleasures-slide-6.png)
![Slide  7](https://bedtime.manufacturaindependente.org/img/pageants-and-pleasures-slide-7.png)
![Slide  8](https://bedtime.manufacturaindependente.org/img/pageants-and-pleasures-slide-8.png)
![Slide  9](https://bedtime.manufacturaindependente.org/img/pageants-and-pleasures-slide-9.png)
![Slide 10](https://bedtime.manufacturaindependente.org/img/pageants-and-pleasures-slide-10.png)
![Slide 11](https://bedtime.manufacturaindependente.org/img/pageants-and-pleasures-slide-11.png)
![Slide 12](https://bedtime.manufacturaindependente.org/img/pageants-and-pleasures-slide-12.png)
![Slide 13](https://bedtime.manufacturaindependente.org/img/pageants-and-pleasures-slide-13.png)
![Slide 14](https://bedtime.manufacturaindependente.org/img/pageants-and-pleasures-slide-14.png)
