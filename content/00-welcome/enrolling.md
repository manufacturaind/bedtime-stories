---
title: Enrolling in Bureaucracktic Bedtime Stories
contributor: Constant
type: text
pages: 1
---

If you want to enjoy the Bureaucracktic Bedtime Stories cycle, there are several ways to subscribe:

At 20:30 CET, you will be presented one story per day, 21 days in a row.
Once you have received all contributions, you will have to unregister. Unless you want to enjoy another cycle !

Subscribe before 1 june 2022, by

- **E-Mail:** you can subscribe to the mailing list here
<https://boucan.domainepublic.net/mailman3/postorius/lists/bureaucracksy.lists.constantvzw.org>
- **Podcast:** add this link to your podcast feed
<https://bureaucracksy.constantvzw.org/podcast.xml>
