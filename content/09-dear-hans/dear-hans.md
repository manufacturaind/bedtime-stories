---
title: Dear Hans
contributor: Lucia Palladino
type: text
pages: 1
---

Lucia wrote a letter to Hans Lammerant, who presented his counter-bureaucratic peace work researching arms trade during the Bureaucracksy worksession. “Bureaucratic practices enable the state or large corporations to observe and act. Through documents and data they can track goods and people, and monitor all sorts of activities. Reason enough for kafkaian fears and suspicions. But nowadays a lot of these administrative traces and open(ed) data becomes available to everyone, turning the world into a large administrative database. This can be used to develop new forms of human rights and conflict monitoring and to create feedback into the political and legal system with independent counter-bureaucracies.”  
The presentation of Hans to which this letter reacts, can be found on [this link](https://bbb.constantvzw.org/playback/presentation/2.3/7e6b3919d077c8a932e5d54848f12201f58efc0f-1607325585230?meetingId=7e6b3919d077c8a932e5d54848f12201f58efc0f-1607325585230).

---

> “Bureaucratic practices enable the state or large corporations to observe and act. Through documents and data they can track goods and people, and monitor all sorts of activities. Reason enough for kafkaian fears and suspicions. But nowadays a lot of these administrative traces and open(ed) data becomes available to everyone, turning the world into a large administrative database. This can be used to develop new forms of human rights and conflict monitoring and to create feedback into the political and legal system with independent counter-bureaucracies.”

dear hans,

this letter is a little bit for you but not only. and it is a letter but also a piece of a dream.  
I am a very shy person and, if you can, take this letter as an attempt to contribute to a discourse that we try to build together into a conversation, me from the position of an alien while looking at your work for the first time, from this other planet where I come from in which even english is clumsy.

thank you very much. your work throws me into another “way” of the world.
I immediately feel guilty for not being engaged in such important issues. really.
and I honestly don't know anything about arms trade. nothing.
and therefore I feel that I am not allowed to say anything about it because this not knowing is equivalent to not having the right to say. I should study for a long time first, over and over again. or at least this is the cultural background from which I come.

but if I linger a little longer, while I think of you, and the photos on the internet, and the tracking ... then other word-bodies and thought-bodies begin to emerge (building other structures of knowledge that I still can't name).  
In chat I asked you where the informations you use come from. as your narration and reality is based on these informations, they become the elements of your hypothesis and thesis. But then I didn't know how to articulate well my own question. It was a real question, without provocation. I was trying to understand from where they come from  to guess what kind of knowledge they produce. Comparing the data and observing the points where these data cross each other is thrilling. they begin to have a strong tendency to become proof of something important in "that" world. And then I start to believe you. But while listening to your story I imagine it in one dimension only. I think of the phantom zone, in superman.

![Dear hans](https://bedtime.manufacturaindependente.org/img/dear-hans.png)  

I think about it, I reflect / refract, and then I fall apart.


I pick up the pieces of the mirror in which I was trapped and I
start thinking about what it would mean and what it would produce comparing
data from different worlds, from different dimensions and different
materials. something tells me that referring to a single system of
knowledge is not enough. and what does it involve to carry out an
operation using data that come from the same matter, dimension and world?  
and what if we compared data from different knowledge systems? what tendencies
of knowledge production would these two operations have? suddenly the idea of ​​having
to deal with only one type of material of information and data makes
me visualize expanses of intensive cultivation of _sameness_.
I wonder if analyzing data concerning a single world deals with
“relationships” _in_ the world, if the world itself is made up of complexity.



I am certainly seduced when I listen to you, I believe you, and yet
still, when I observe the one-dimensional image of the phantom zone, I
strongly desire to enter at least one arm inside the square in flight
in the universe and pass to the other side, crossing worlds.

and related to that, how much is:

```a tank           +
a photo on the web  -
a flood             =   ?
```

warmly,

lucia
