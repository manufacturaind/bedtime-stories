---
title: Top of the list & (Human centric) frequencies
contributor: Elements and Animals, Wendy Van Wynsberghe
type: audio, image, text
media: frequencies.mp3
pages: 12
---

Often in a bureaucracktic procedure, your place on the list is of great importance, whether it goes from access to housing to getting an asylum procedure to advance. These lists sometimes get mythical and mystical properties. Do they exists? Is there the ultimate list? Can we see this list? Can we change or influence it? Melt it?
As humans on the obscure side of the list we don't get access to the order. But apparently as animals, the list access is better [1]. But then, how to influence this list if you don't communicate in human frequencies?
Implement the rights of nature into the constitution? [2] What does nature write?

`>> text and sound are preferably read sequentially`

Fragments of [1]

![](https://bedtime.manufacturaindependente.org/img/species-and-lists-1.png)

We analysed LIFE projects focused on animals from 1992 to 2018 and found that
investment in vertebrates was six times higher than that for invertebrates (€970
versus €150 million), with birds and mammals alone accounting for 72% of species and
75% of the total budget. In relative terms, investment per species towards
vertebrates has been 468 times higher than that for invertebrates. Using a trait-
based approach, we show that conservation effort is primarily explained by species'
popularity rather than extinction risk or body size.

![](https://bedtime.manufacturaindependente.org/img/species-and-lists-2.png)

_Tables – Queries_

We extracted information on the amount of funding allocated to various species
using the LIFE projects database (<https://ec.europa.eu>; accessed between February
and May 2020). Note that we focused here on a species-level conservation approach
[9], in contrast with other more general conservation measures (such as socio-
ecological approaches [15,16] and others [17]) that are only indirectly covered by
LIFE projects. We first filtered LIFE projects specifically aimed at species
conservation, using the query STRAND = ‘All’; YEAR = ‘All’; COUNTRY = ‘All'; THEMES =
‘Species’; SUB-THEMES = ‘Amphibians’; ‘Birds’; ‘Fish’; ‘Invertebrates’; ‘Mammals’;
‘Reptiles’. This query resulted in 819 LIFE projects that met the search criteria. A
second query focused on THEMES = ‘Biodiversity issues’; SUB-THEMES = ‘Ecological
coherence’; ‘Invasive species’; ‘Urban biodiversity’, which matched an additional 298
LIFE projects.

[...]  
The top 30 invertebrate species, in terms of LIFE funds allocation, were
coleopterans [beetles and weevils], butterflies, dragonflies, and a few molluscs
(figure 1c); but this funding is miniscule, given invertebrate diversity and the
current rate of declines across diverse habitats [37–39]. Even within invertebrates,
the biases are notorious, with widespread, large, and/or colourful species being
dominant [40]. Among vertebrates, 54% of species covered by LIFE projects were
birds (accounting for 46% of the budget allocated) and 18% mammals (24% of the
budget), with only 8 out of the 30 most protected vertebrate species not
belonging to these two groups (figure 1c).

[...]  
However, based on past investment and taking the Habitats Directive and LIFE
projects as a proxy, it seems likely that a few charismatic species will receive
almost all the attention in the context of species-based conservation funding.
When striving to assign the status of protected area to 30% of the EU's territory
and halt the documented trends in species extinction [9], especially the silent
extinctions of invertebrates [38], it is essential to overcome the prevailing
taxonomic [34] and other [46] biases. We, therefore, propose a roadmap for more
equitable species-focused conservation investments in the next decade.

Fragments of [2]

— the politics of the rights of nature were gradually forged in Ecuador. “When we
approved the rights of nature in the Constitution, this process implied a reflection
on what exactly nature is,” explained ecologist Esperanza Martínez: For science it
is one thing, for the indigenous people another, for law another one, and for
capitalism yet another. For capitalism nature is environment: a place where one
extracts resources within certain limits. The indigenous people have a distinct
notion: nature is not only the ecosystem, but also spiritual beings. In the case of
biological sciences, it depends from which scientific paradigm you depart: are human
beings included inside nature or not? So we are not speaking about the same thing.
When we decided to adopt the term Pachamama, it was foremost an act of
acknowledging the wisdom of those who are so closely tied to the earth. It was also
a critical act in relation to the classical notions of environment and nature.

[...]  
Pachamama — usually interpreted as Madre Tierra, “Mother Earth," a mythical deity
entity that is omnipresent in Andean indigenous cultures — was the chosen concept
to guarantee that Amerindian cosmologies were politically represented withinconstitutional law. “If modernity has adopted a single paradigm, one single
rationality, one sole model of nature,” Martínez concluded, “what we are saying is
that there is not only one but many, as many as there are cultures.”


Credits:

**(Human Centric) Frequencies** was recorded in 2020 and 2021, somewhere in Houyet
and Schaarbeek, Belgium.

[1] **Towards a taxonomically unbiased European Union biodiverity strategy for 2030**  
<https://royalsocietypublishing.org/doi/10.1098/rspb.2020.2166>

Authors' contributions:

M.L.-L., N.R., R.S., S.M., and V.P. conceived the idea. N.R. mined data from LIFE
projects. R.C. extracted data from IUCN and the cultural value of species. N.R.,
R.S., and S.M. extracted species body size from different sources. S.M. performed
analyses and prepared figures. R.S. and S.M. wrote the first draft of the paper. All
authors critically contributed to the writing of the paper through comment,
additions, and revisions.

Competing interests  
We declare we have no competing interests.

Funding  
R.C. is funded by the Helsinki Institute of Sustainability Science (HELSUS) and the
University of Helsinki. S.M. is supported by the European Union's Horizon 2020
research and innovation programme under grant agreement no. 882221.

Published by the Royal Society under the terms of the Creative Commons
Attribution License <http://creativecommons.org/licenses/by/4.0/>, which permits
unrestricted use, provided the original author and source are credited.

[2] **Nonhuman Rights**  
Contributor: Paulo Tavares  
in: Forensis: the architecture of public truth, edited by Forensic Architecture  
<https://www.paulotavares.net/nonhuman>
