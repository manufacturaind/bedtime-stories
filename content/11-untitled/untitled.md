---
title: untitled
contributor: \_
type: text
pages: 1
---

This is a nightmare.

It starts with a picture:

A hallway inside a modern building. There is a long white paper on the floor stretching into the distance with several columns filled with words, but they are not legible. A few people in bureaucractic clothes are standing on the side of the list. They are not touching the paper except one person further away in the hallway. Their shadows fall over the list while one of them points a mobile phone at the paper and takes a picture or maybe answers a message.

There is a caption:

"100 meter long list with the names of the 17306 people drowned in the #Mediterranean in an attempt to emigrate. The list was put on the ground so European MPs are forced to walk on it when entering parliament."

It is not a nightmare.

Facing the list. Unrolling the long, very long white paper. Reading line after line. Spending two hours with it, or as long as it takes.

Taking a picture. For later.

Walking on the list. To go somewhere else.

Voting for the reinforcement, management and security of EU's external borders.

Saying: "The ultimate goal is to sustain a safe and secure area of freedom, security and justice." Repeat.

Searching the list for people you might know. *1 result found*

Thinking of the ones you don't know. *11.086 'unknown' found*

Deciding how many columns the list should have, and what vocabulary to use for describing these different deaths.

**add 45.000 rows**

Thinking to update the caption with the latest number. *40.555*

Worrying about the right number. Dying, or dying by drowning.

Wondering how to count, who counts. Who knows what happened?

Deciding to stop updating the document.

Adding names.

Always more rows, never less, never none.
