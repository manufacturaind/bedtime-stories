---
title: Whispered Meditations on the Algorithmisation of Bureaucracies
contributor: Loup Cellard
type: text
pages: 2
media: meditations.mp3
---

### Introduction

The whispered meditations on the algorithmisation of bureaucracies is a collection of thoughts reflecting on the technological transformation of States taking place through a data-saturated regime of computational procedurality. The meditations are an occasion to reflect on the figure of the “algorithm” as a material technology but also as an invasive rhetorical agent spreading through bureaucracies. These thoughts take the form of: dark mantras; poetic speculations; principles for refining our analytical attention; curious statements waiting for further investigations; problematic questions and other paradoxes. The meditations interrupt the streams of massive data we are living in by forcing us to stop our thinking on dense but incomplete proposals. We invite readers to let their streams of consciousness navigate between the different meditations grouped together by themes. Alternatively, readers may also want to whisper these meditations to their partners or favorite line managers.


### 1. General Analytical Attention:

1.1. A bureaucratic algorithm is the complex translation, adaptation and codification of legal rules.

1.2. Be careful, a warning is breaking the loop: “human and non-human entities bring energy, warm randomness and relational contingencies”.

1.3 The bureaucratic algorithm problematically transformed qualitative attributes into quantitative measures.

1.4. Divided attributes of (non) humans create a grammar feeding the process of automation.

1.5. There is no algorithm but a tentacular infrastructure of procedurality. A data(set), an information model and an algorithm always operate together.

1.6. Mundane bureaucratic algorithms get individuated and personalised in frictions with software layers.


### 2. Discourses:

2.1. Seduced by the sublime metaphor of the algorithm, the Prince is locked in its own secret machinations.

2.2. The bureaucratic algorithm is a figure of rhetoric stuck in a fable and acting as a fetish.

2.3. Figuring algorithms is an attempt to reconfigure the imaginaries of systems, procedures, orders and biological recursivity.

2.4. If you are tired of endless debates about “black boxes”, feel free to click on the “reset” button at the bottom of these meditations.


### 3. Problematisation:

3.1. Bureaucratic algorithms act as delegates compensating the moral deficiencies of civil servants.

3.2. The bureaucratic algorithm (re)opens policy problems more than it resolves them.

3.3. It matters to know which expertises and persons are legitimized to formulate the problems and solutions to algorithmic harms.

3.4. The excess of engineering solutions to cope with algorithmic harms is a spectacle naturalising the expansion of their use in any corner of our lives.

3.5. It is with our diverse hands, tools and methods that we will be able to navigate the spaces of computational problems.


### 4. Organisation:

4.1. Bureaucracy is the first of all artificial intelligence.

4.2. Within and beyond a bureaucratic algorithm we find hidden bureaucrats, dismantled social groups and overlapping organisations.

4.3. The bureaucratic algorithm does not belong to public or private organisations, it is a “by-default” mechanics of ordering.

4.4. Internally, the bureaucratic algorithm is virtually too big, externally, we only see the wandering of inputs and outputs.


### 5. Temporality:

5.1. The bureaucratic algorithm is old but vigorous — we need to envision the State as a living archive.

5.2. Please, pay attention to the (non) recursive rhythm of this algorithm.

5.3. Surprised by all these contestations, it is difficult to know when we have to care for “black boxes”.

5.4. What needs to be made accountable is never a “thing”, but an excess of relationships coming one after another.


### 6. Subjectivities:

6.1. Our personal stories of bureaucratic encounters are full of grey mazes and grotesque kings.

6.2. The bureaucratic algorithm triggers many identity switchings: I am a singular citizen, a standardised user and a datafied subject filled by attributes coming from others.

6.3. The bureaucratic algorithm is haunted by computational methods of optimisation and personalisation.

6.4. Late at night — in the shadows of missing data — we are fugitives dreaming of the uncomputable.

6.5. If at this stage you do not see the algorithm, can you hear the never-ending loops of waiting room music?

---

**\*Acknowledgements:** I would like to thank the participants of the Bureaucracksy work session that inspired these meditations. My gratitude to Francis Hunger (Bauhaus University, GE) who inspired me meditation 1.5. Some of these thoughts also emerged in discussions with Danny Lammerhirt (Siegen University, GE), and Michael Castelle (Warwick University, UK) especially meditation 4.1 is from him.
