---
title: Prison Ordinance
contributor: Muslin Brothers
type: audio
media: prison-ordinance.mp3
---

Hello !

Tonight we are going to share with you a list of rules we have curated.
This is a collection of prison ordinances and other regulations that outline a partial protocol of the style of incarcerated bodies. They are quoted from diverse state laws, international conventions, and specific correctional facilities. Originating from foreign languages, these articles have been translated by a language “know it all” oracle and might contain light grammar issues.

We invite you to listen to what shapes the appearance or disappearance, to envision the color, cut and choreography of overlooked bodies, wearables, spaces and objects of incarceration.
