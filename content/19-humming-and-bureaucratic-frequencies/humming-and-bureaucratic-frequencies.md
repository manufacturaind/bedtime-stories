---
title: Hum bureau (Bureaucratic frequencies)
contributor: Eric Snodgrass
type: audio, text
pages: 7
media: hum_bureau.mp3
---

Notes on humming and bureaucratic modes of perception/counter-perception
gathered during the Bureaucracksy event and discussed further in the accompanying sound file.

---

**hummingbird clock**, Lawrence Abu Hamdan

*For over 10 years the UK government has been using the humming sound of
the electrical mains as a surveillance tool and ***forensic*** clock to
authenticate recordings — to determine their time and date, and whether
they have been edited or otherwise altered. They call this technique,
invented by Dr Catalin Grigoras, ***“electrical frequency network (ENF)
analysis.”*** It can be effectively used as a ***time stamp*** for
almost any recording made within earshot of electricity, which ***is
always — almost silently — humming***. This use of the sound of the
electrical grid as a fingerprint of the nation's time has only ever
been used by the police as a tool of state level surveillance, ***and
yet everyone has access to this same buzz...*** As the
secondhand turns around The Hummingbird Clock like a ***seismograph***
it draws the line of the UK's fluctuating mains current and this
pattern of fluctuation is being recorded and archived every second
***24/7***... Digital recordings almost always have ***mains hum*** on
them, either because the device was plugged in to the mains or because
it inducts it off nearby cables, lights and appliances in a room. The
Hummingbird Clock is able to automatically match the fingerprint of the
mains hum on a given recording with it's ***database of the buzzing
mains,*** and therefore tell you exactly when the recorded event
occurred or if the recording itself has been tampered with and edited.
The archiving of the mains power supply begins on the 7th July 2016 so
we can only analyse recorded events made from this date forward.*  
<http://www.hummingbirdclock.info/about/what-is-the-hummingbird-clock>  
-- buzzing mains as humming, (forensic) frequencies  
-- time(stamp) element of this frequency  
-- **hum perceived as constant yet unique and indexical**, as in some
senses with bureaucracy  
-- Technical - Frequency related (top item - Hummingbird clock): To find a cable
in the wall, you use a coil or an inductor, which is amplified to the sound
realm. To be able to find the utility frequency of 50 hz. Electricity
flowing becomes sound.

<https://en.wikipedia.org/wiki/Utility_frequency>

**bureaucratic frequencies**, hummings  
the waiting room clock, ticking and ticking  
steps in corridors, shufflings, tappings, small ongoing echoes,
**reverberations**  
typing, pens and ballpoints scrawling, rolling over surfaces of
**inscription**, **sounds of bureacratic input**  
the oral **“hmmmm”** of a bureaucrat taking forward and examining one's
case/query/answer  
**heightened senses** to such sounds and sensory inputs in extended and
enforced forms of waiting within bureaucratic spaces  
the ongoing **din** of electricity, lights, generators, cicadas...
**punctuated by moments of bureaucratic activity and execution** - e.g.
the stamp of approval/denial, opening/shutting of a door, calling of a name


Tina Campt on serial bureaucratic grammar + **quiet frequencies**  
*...The albums ***serialize and aggregate ***the criminal body through
photographs that ***standardize*** individuals and ***assign*** them a
number. ***Organized externally and internally*** by the date of the
photo session, each page of the album assigns a criminal identity and
visual uniformity indexed through the following information:*

*name*  
*serial number*  
*clothing*  
*pose*  
*sequential date of photo*  


*A fifth ***haptic temporality*** is the temporality of my own
***archival contact*** with the images and the albums. The albums have
***a sensuousness that, to me, felt almost illicit***. It felt wrong to
have access to ***intimate details*** of bodily markings, illnesses,
whippings, closest relatives, attempted and successful escapes. It was
overwhelming to track these men from one ledger to the next and resist
***the seductions of the data and descriptions the albums contained***.
In fact, it was frustratingly easy to succumb to the original intent of
the archive: ***to reduce the individuals to statistics***. And it was
literally dizzying to shift back and forth between the four tables
Ericka had assigned me, on which I had placed the three ledgers
corresponding to the photos of inmates in each album. ***As I wheeled
myself on a rolling chair from one table to the next*** to connect each
black or brown face with the compromised details of an identity assigned
to him through the categories of the ledgers, this ***haptic
encounter*** forced me ***to encounter them through a different sensory
modality*** in an attempt ***to resist the silencing effects of the
serial bureaucratic grammar of the archive***. It forced me to attend to
the ***quiet frequencies*** of austere images ***that reverberate
between images, statistical data, and state practices of social
regulation***.*  
-- reverberations, encountering through different sensory modalities,
quiet soundings and frequencies that have been flattened out through
imposed and enforced bureaucratic standardizations/violences
-- figure/ground nature of such frequencies


humming as **blocking out**  
*“We no longer like to think about bureaucracy, yet it informs every
aspect of our existence. It's as if, as a planetary civilization, ***we
have decided to clap our hands over our ears and start humming whenever
the topic comes up.”*** (David Graeber, The Utopia of Rules, p.5)  
--humming as denial


**humming along**  
--humming along **when things work**, keeping things humming along,
when **operations and procedures being carried out experienced as
working and agreeable** for doing so, humming with the “utopia of
rules... the **regularity** and **predictability** of bureaucratic
procedures, and the **routiniza­tion** of force” (Graeber), happy
humming bureaucrat when bereaucratic apparatus operating as should
(Kafka execution machine operator), a “stable” hum of a machine
**executing as it should without much noticeable audible stress** on the
machine  
--but see also humming as blocking out, humming **so as not to think
too much** about what the procedure is actually achieving, nervous
humming, pensive, willingly oblivious or from simple **habit** having
carried out the procedure so many times  
--**sigh of relief** after a successful negotiation through a
bureaucratic encounter - **sigh as contrast to humming**  
--my driving instructor humming songs to himself whenever i was doing
something wrong during driving lessons  
--**humming along to music, work**, etc. **attunement** with the
rhyhtms  
--**physicality of humming and sound(waves)** - **bones** in the ear,
rattling of skull while humming, **physical encounters with objects** -
humming while feet walking across the hospital corridor, while operating
a physical machine/object  


Elin Már Øyen Vister
(<http://nidacolony.lt/images/docs/February_2020/DOTT-pub-small2.pdf>)  
“The **deep frequency** of the oceanic hum is shaped by the acoustic
realities it is born out of; the geology below the surface and the
global weather systems. The hum enters our bodies as sonic vibrations;
through our ears (unless we are hearing-impaired) **and through our
bones. **The low frequency hum **even massages our guts**. Our guts are
even making sound themselves. A complex, ever-changing organic hum
carries potentials for healing. Mammals, such as us humans, are born
into a thirty-seven degrees Celsius ocean and begin to listen to the
soundscape that surround them in their mother's womb when their ears
become 'ripe' around the fourth month. Their soundscape includes
**filtered sound from the 'outside', and inner 'noise'**, such as the
hum of the bloodstream and their mother's digestive system, as well as
the heartbeat and breath.”  
--ryhthms of a bureaucracy humming, heartbeats, bloodstreams, breaths,
digestions, guts\... how these hums enter bodies in bureaucratic
encounters and their acoustic realities  
--Pauline Oliveros deep listening in relation to humming\... deep
humming  
--“Oliveros was born in Houston, Texas, on May 30 1932. Her mother was
a piano teacher and songwriter, her father a dancer. Her most vivid
memories of her upbringing were acoustic --- the crackle of her father's
short wave radio, birdsong in the suburban trees, the blending of her
parents' voices with the hum of the engine during car journeys.”

ASMR hums  
ASMR 4 bureaucrats  
...


figure/ground, signal/noise: bureaucracy + breakdown  
--bureaucracy and **breakdown**, same with writing on ****black box****
and also infrastructural breakdown  
--Graeber on bureaucracy telling no stories except when not working:  
“As a corollary: ***in fantasy, as in heroic societies, political life
is largely about the creation of stories***. ***Narratives are embedded
inside narratives***; the storyline of a typi­cal fantasy is ***often
itself about the process of telling stories***, interpreting stories, and
creating material for new ones. This is in dramatic contrast with the
me­chanical nature of bureaucratic operations. Administrative procedures
are very much not about the creation of stories; ***in a bureaucratic
setting, stories ap­pear when something goes wrong. When things run
smoothly, there's no narrative arc of any sort at all.”***  
--the process of telling stories, ***the (non-heroic) machine designed
not to tell its own stories***, ***annotator and accountant rather than
storyteller***  
--ground becoming figure (not quite) in extended enforced waiting or
negotiation with bureaucracy  
--Shahram Khosravi on waiting, being ***out
of time*** with standard waiting and living times, “belatedness”, ***the
oppressiveness of the hum that ignores and denies***
