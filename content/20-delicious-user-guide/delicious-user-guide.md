---
title: Delicious User Guide
contributor: Jonathan McHugh
type: text, image
pages: 18
---

#### Table of Content

- [Part 1: Introduction](#part-1-introduction)
- [Part 2: Mermaids Folksong&me](#part-2-mermaids-folksongme)
- [Part 3: Delicious user guide](#part-3-delicious-user-guide)

<div class="break-page">&nbsp;</div>

## Part 1: Introduction

I have had a lot of misgivings regarding the allure of 'Web 2.0' services - they always seemed so deficient to me.

In 2007, after years of researching and writing about the power of information systems I had the opportunity to have hands on experimentation - in this instance for providing monitoring infrastructure and monitoring of news/public affairs articles.
The social bookmarking service, Delicious seemed the perfect vehicle for documenting and annotating. While I could have used Word documents or a Blog to document it lacked depth.

Most notably, there was no recursive searching of tags. Having the ability to search only one tag meant that people could not dig deep into a story. They would have to make a choice between choosing the tag with the closest precision (so that the searching within the tag was less - though with risks that the tag was not kept) or choosing the tag with the broadest categorisation (with the trade off being that more time would be spent looking through results from the tag). The ability to be able to inspect one tag and see a list of other tags which exist within their selection (and the number) was liberating. This reward provided the encouragement to annotate tags properly.

Naturally, over the months the scale and sophistication of the notes and volume of different tags created the need to improve myself to improve my workflow and navigation across the knowledge repository. An annotation tool was launched with CTRL-D, presenting the relevant URI, a 1000 character box and tag options. Within the tag field, the limitations of using only A-Z characters became increasingly clear. Always an enthusiast for touch typing, I hit upon the idea for using the non-alphanumeric characters (found just to the left of the RETURN key) - this seemed useful for theming tags. For example, hitting '[' would provide a list of politics related tags and ']' economics. The "'" button would carry locations, with the number of single quotes providing abstraction levels.

As well thought out a product Delicious was, I considered the 'social' aspect of its bookmarking utility intriguing and revolutionary. Annotations for a specific URL intersected, as multiple users were capable of providing a note with differing motivations or emphases. If I looked as an annotation, it would provide their note, tags. It was an entry point into other peoples heads in way which the ego driven and synchronous modes of FaceBook and Twitter could not touch - it was centered around the need to understand rather than be 'acknowledge oneself', with a precision to interrogate the facets of the individuals knowledge (if valuable rather than their complete form).

It was not immediately rewarding poring through the individual RSS tags of users: I would have to ignore some tags as being too noisy, too voluminous (the fire hose) or not entirely relevant to my employers requirements. However, because I was annotating all the RSS tags of other users I was getting very clean lines of information. Becoming aware of the distinctions between taxonomies and folksonomies at a practical level (over ~ 10,000 user profiles) have given me a deep understanding regarding what language is and how it is plastic, tactile and inherently ambiguous.

The glue was to integrate the RSS feeds with Yahoo Pipes (Delicious was also one of their products), which was a WYSIWYG way of mixing content. The tool was a bit of a toy, designed to allow anybody to mix content in different ways, using parameters. My account had classified RSS feeds as RollingNews; Delicious had recursive searching of tags; Delicious provided 'private' RSS feeds to be called - all this funneled into a tool with aggregating and recursive functions provides a workflow more powerful than many would expect.

As a consequence a collection of ~200 people for unconnected reasons and perspectives were pumping information that would have been beyond the capabilities of a single researcher or (inhouse) a medium sized enterprise. I still believe in the logic of having tools and communities to be supplied by ones own civil service. Later, I discovered that 200 is both the optimum number for both Athenian democracy gatherings and social media interactions - small world.... The scale and granularity of information I collected exceeded anything publicly available on the internet for the knowledge domain. Naturally, there could be 'knowledge rot', as users dropped off the map. However, over time the quality of information found was improving, as I was reaching comprehensiveness in terms of topic areas and the communities which interacted with them. Future colleagues appreciated the work and the scale. The hunt for knowledge leaders and tethering them into a cohesive infrastructure was rather addictive.

Alas, the golden age was declining around the time I was moving onto a different employer. Yahoo was a golden boy of the early Internet but its financial success was not a consequence of it understanding its services potential - rather than providing a well oiled  machine for monitising the Internet (see this article for context http://www.paulgraham.com/yahoo.html). It did not realise how awesome the Trinity of Delicious, Yahoo Pipes and Flickr were. It sold off Delicious to some bullshit enterprise which altered its back-end so that tags beginning with non alphanumeric characters did not work (worse if killed the thousands of tags uncerimoniously). Eventually it gave up, neither operator had an idea for (directly!) commercialising it. The Internet is turning well shit, most services we are encouraged/required to use are exploiting us. This was a more innocent apex where there was a feeling that knowledge systems could be around topics rather than tribes.

Monitoring can often be mistreated as a treadmill activity, which when mixed with communications and marketing can result in people propagating to one and all. This distinction has always made me seem very alien to most Comms professionals, they have a cognitive dissonance regarding how their role should be structured. My Cover Letters must have been written in Kriptonite, as my activity and justifications would not even arouse the curiosity for a 1st round interview. (I suppose hammers only seek nails).

I eventually spent years mastering Drupal (5-7), creating
(uncommercialised) knowledge portals to represent such knowledge
hubs. Only later on did my autodidactism direct me to the advantage of
proper coding to interpret information and knowledge flows
effectively. The workflows for generating knowledge and identifying
leaders is still in my apparatus, though I have usually only directed
them towards things such as activism. The burning need to represent the
complexities of knowledge and purpose continue, with me devoting about
7+ years with folksonomy techniques - after eating a lot of dog food it
mutated into Qiuy, a 'Recursive Modeling Language', which will remain
molten for a long time will eventually represent for me a concrete
representations and delineations regarding the intersection of state and
purpose.

<div class="break-page">&nbsp;</div>

## Part 2: Mermaids Folksong&me

```
Ate#se, (oo]wind bre#z, %%%%%%%%%%%%%
RSS feeds abundant, %%%%%%%%%%%%%%%%%
Forfar,sure!fo]ksonome#z, %%%%%%%%%%%
Whoso ]ist t2-hunt, %%%%%%%%%%%%%%%%%

Before de-RTRN k#yz, %%%%%%%%%%%%%%%%
Extend-ripp]e,down-dune, %%%%%%%%%%%%
de]imtrs-und-parenths#yz, %%%%%%%%%%%
beech@night,beach@noon, %%%%%%%%%%%%%

A]phas make rough-grain, %%%%%%%%%%%%
-(ast- into the-sure, %%%%%%%%%%%%%%%
Re(ursive]y sandy-(hain, %%%%%%%%%%%%
Annotating#more&more, %%%%%%%%%%%%%%%

[po]itics {(ulture a]] yeah! %%%%%%%%
She]]#4my ]earning senses, %%%%%%%%%%
Over 1+kay tagstid-ing ther#, %%%%%%%
Shear(hre a]] re]ent]ess, %%%%%%%%%%%

Ripp]ing a]] th`m (ora] feeds, %%%%%%
Another (#mmunity is th#r` be]ow, %%%
Piping the f]ows form-e-needs, %%%%%%
Demar(ations 4d#se in the kn#w, %%%%%

B#gg]er, b#gg]er. Putput. %%%%%%%%%%%
winds-(#mmercia] pivot (o]d. %%%%%%%%

    Kite (r#sher r#t. %%%%%%%%%%%%%%%%%


is-de sm]]yfishin' f]eet. %%%%%%%%%%%

B#gg]er, b#gg]er. Putput. %%%%%%%%%%%
Benchmarking against bo]d. %%%%%%%%%%
Unput put *#gg]er snot. %%%%%%%%%%%%%
Feedail #db bawbag s]eet. %%%%%%%%%%%

T$$#much#noise, 1++ pi]]er peari$h#t,
Water-in-symmetry awash-runes, %%%%%%
unbanktd poise, purput wyatt-cos#t, %
Bnnked know]edge on-da- dunes, %%%%%%

M#rmaids an#otations s]eep, %%%%%%%%%
whearto be ]ent in scope, %%%%%%%%%%%
I (ast and s(im ank]e deep, %%%%%%%%%

    memory of urchins' hope, %%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%% Jonathan McHugh
```


<div class="break-page">&nbsp;</div>


## Part 3: Delicious user guide

### Part One: Accessing the System

1a) To log in visit:
[**https://secure.delicious.com/login?jump=ub**](https://secure.delicious.com/login?jump=ub)

![Delicious user guide](https://bedtime.manufacturaindependente.org/img/delicious-user-guide-01.png)

The user name is: squirreltrumpet
The password is: PASSWORDEXAMPLE

<span class="text-red">Please take care to check whether you intend to keep the account signed
in for two weeks. I recommend that it should only be checked for office
computers and private laptops when in the house or office.</span>

<span class="text-red">to log into the site in order to then log yourself out, as carelessness
risks data theft.</span>

<span class="text-red">The automatic log in system is for two weeks, so similarly try not to
forget the password either!</span>

![Delicious user guide](https://bedtime.manufacturaindependente.org/img/delicious-user-guide-02.png)

1b) Delicious operates a clean design to enable you to explore safely
without hassle. However, the system can easily fail, as this system is
depended on effective social processes. People being inconsistent or
failing to work according to the same guidelines is likely to create
losses of information and therefore an ineffective system. So continuing
reading on (particularly the section on tagging), referencing the
material in the future and updating this guide, as effective use should
reap dividends.

### Part Two: Searching for Information

![Delicious user guide](https://bedtime.manufacturaindependente.org/img/delicious-user-guide-03.png)

2a) The top bar (above) serves as the general dashboard for searching.
You can click on the search piece *Type another tag *to find key tag
words. However, this may always find the information you require as a
result of human input error (this will be discussed later). Sorting the
information below that bar by *Most Recent, Alphabetically *or *Reverse
Order* should enable to sort the information more conveniently,
depending on your needs. The <span class="inline-image">![Delicious user guide](https://bedtime.manufacturaindependente.org/img/delicious-user-guide-04.png)</span> information on the left hand side
determines how much information is shown on the links displayed below.

![Delicious user guide](https://bedtime.manufacturaindependente.org/img/delicious-user-guide-05.png)

2b) Clicking on *SquirrelTrumpet* (or also the very top rear the right
of the screen) will enable you to return to the main section if you have
decided that you need to search again. Next to *SquirrelTrumpet* can be
grey boxes containing tag choices that you are currently searching with.
To remove these search choices, click on the *x* next to the terms.

The right hand bar (right) should be your first point of call for
navigating this system, as its folder based ordering structure should
guide you gently to your information, as well as reinforcing the thought
process that exists so that you input the most accurate data

2c) To find existing tags first click <span class="inline-image">![Delicious user guide](https://bedtime.manufacturaindependente.org/img/delicious-user-guide-06.png)</span> in order to open
and close the available information within each category. The key two
folders are *Tag Bundles *and *Related Tags*.

Opening *Tag Bundles* (right), you get a range of categories which
collect information (again organised within information groups as tags).
Clicking <span class="inline-image">![Delicious user guide](https://bedtime.manufacturaindependente.org/img/delicious-user-guide-06.png)</span> next to EU Institutions provides a list of internally collected
tags considered as being relevant to XXXXX in relation to EU public
affairs.

2d) The main bundle groups are:

-   _EU Initiatives_
    -   Covering key European projects
-   _EU Institutions_
    -   Covering the major EU political pillars and relevant EU
        institutions
-   _Events_
    -   Covering key meetings and events
-   _Music Content, Labels, etc_
    -   Covering information on record labels
-   _Music Distributors_
    -   Covering P2P sites, radio stations, social networks, and key
        media players
-   _Music initiatives_
    -   Covering non technologically based issues
-   _Music Public Affairs_
    -   Covering key organisations and topics in public affairs
-   _National Initiatives_
    -   Covering key debates in local countries
-   _News Aggregators_
    -   Covering all third party bookmarks contributing to
        SquirrelTrumpet
-   _News Journalists_
    -   Covering a list of journalists or references that have had their
        contact details recorded
-   _News Sources_
    -   Covering all the sites contributing to SquirrelTrumpet
-   _Office Information_
    -   Covering information on hotels, travel, etc
-   _Technology Companies_
    -   Covering technology based companies not associated as content
        providers or disruptors of music
-   _Technology Public Affairs_
    -   Covering key debates that have a technological slant, as well as
        physical and digital product information and debates


2e) Click on *EUCommission*. This will pull up a list of
all the articles or links containing the tag *EUCommission* (right).
When selecting <span class="inline-image">![Delicious user guide](https://bedtime.manufacturaindependente.org/img/delicious-user-guide-08.png)</span>, you should be able to see the date the link was
created, the title, short description, all of the tags that each link
contains and whether other users across the Delicious network also
happen to have the link in their bookmarks.

![Delicious user guide](https://bedtime.manufacturaindependente.org/img/delicious-user-guide-09.png)

There are 36 links in this case, which may be too broad for research
purposes. With Delicious it is possible to search within two terms at
the same time, enabling you to narrow down your search through creating
connections.

There are two ways to do this, either clicking on one of the boxes
inside one of the links at the bottom of each link box.

2f) The other method is to use the *Related Tags *section on the right
bar (right). It contains all tags which have a connection with the tag
*EUCommission*, as indicated by the image "+" before the title. In this
example, click on *copyright*. You should now be reading all the links
associated with both the European Commission and copyright.

It is possible also to narrow down the search in other ways or even
deeper. The *Related Tags *section is broad, as it contains all related
information. This may act as a distraction but can also allow you to
search by location, source, committee or whether the information is a
link to a single story or a collection of information (more to be
discussed later). Play around with the system to familiarise yourself.

As mentioned before, if you make a wrong turning click on
*SquirrelTrumpet *or an "x" on the term that is no longer required.

2g) If you still struggle to find what you are looking for try opening
the *All Tags* folder. Then use your browser's find option to find the
term. Otherwise attempt to use different terms. The Tagging guide will
attempt to provide a grammatical system for labelling information, as
well as highlighting where errors may have occurred. As a result, it is
worth reading to aid your searching. If after consulting with your
colleagues you can not find the information try reverting to more
traditional forms of finding information, remembering to leave
breadcrumbs so that future research is more fruitful.

### Part Three: Adding Links

3a) There are two methods to adding links to Delicious.
The first method is to <span class="inline-image">![Delicious user guide](https://bedtime.manufacturaindependente.org/img/delicious-user-guide-10.png)</span>
add a tool to your Internet browser. To do this visit
[**http://delicious.com/help/tools**](http://delicious.com/help/tools)
and click on the appropriate link. This will provide you with three
buttons (right). The left opens SquirrelTrumpet, the middle button opens
the bookmarks and the right button opens a box to record the page you
are currently using to SquirrelTrumpet.

![Delicious user guide](https://bedtime.manufacturaindependente.org/img/delicious-user-guide-11.png)

Alternatively, when in SquirrelTrumpet you need to use the blue box on
the right hand bar of the website (right). To save a link click on *Save
a new bookmark. *Next, insert the URL and then *Next*.

3b) The next screen (right) provides a form for creating
the background information behind the link you are recording, including
Title, Notes, Tags and whether the link is public to the world or not.
<span class="text-red">Remember, data security is imperative as we do not intend to release
this data publicly. As a result, always make sure that the ***Do Not
Share*** box is ticked, so that only SquirrelMail users can access the
material.</span>

![Delicious user guide](https://bedtime.manufacturaindependente.org/img/delicious-user-guide-12.png)

Tags are a collection of key words chosen by the user to provide key
descriptors so that they and others can access them later. This system
has many pros and cons to it, although a system which works in a uniform
fashion is likely to be fruitful. Below are some key rules which will
help you to maximise this tool.

**Spacing, Plurals and Errors**

-   Unlike most other tag based systems Delicious does not separate tags
    by commas, relying on keyboard spaces. As a result, "*EU
    Commission*" would produce two tags, "*EU*" and "*Commission*".
    There are many alternative solutions to this but the recommended
    approach is to group the words into one, making the first letters of
    each word capitalised. In this case the correct tag would be
    "*EUCommission*".
-   Unless explicitly shown, illogical or only existing in the singular,
    assume that tags are used in the plural. Therefore, "*radio*" and
    "*EUCommission*" are used in the singular, while *Glastonbury* would
    exist under "*festivals*", as it belongs to a group which contains
    numerous festivals.
-   Please check the spelling carefully, as "*EEUComissiion*" creates a
    tag completely separate from "*EUCommission*" and is unlikely to be
    found in searches until the error is corrected.

**Source**

-   It is important to include the source of the link so that it would
    be possible to choose the source of material that the link belongs
    to. In this example the piece comes from *The European Voice*. In
    this example "+*EuropeanVoice*" is typed as the source descriptor.
-   There is a code to highlight the source and denote whether it will
    be original or merely aggregating news stories from other places. If
    the article or link is to original content, the source descriptor
    would contain a "+" at the front of the tag. If it links to
    unoriginal content, then a "-" preceeds the tag.
-   Sometimes it is unclear what to include from some websites, as
    *www.nortonspeel.wordpress.com* could be either *NortonsPeel* or
    *Wordpress*. Similarly, *www.europarl.europa.eu* could be *Europarl*
    or *Europa*. Dealing with such situations, it is best to bear in
    mind whether the sub section of the website operates as part of a
    collective or individually from the rest of the site. In the first
    example, *Nortons Peel* operates independently of *Wordpress*, so
    "-*NortonsPeel*" would be chosen (the "-" is used because it
    aggregates stories, rather than creates them). In our second
    example, even though *Europarl* operates independently or *Europa*,
    it's reporting still fits within the remit of EU institutational
    related affairs so becomes lumped with the tag "*Europa*".

**Location**

-   It is useful to highlight the geographical relevance of any story
    but not helpful to provide historical settings. So for example,
    *MIDEM* is held in *France* but its effect is *EU* wide. As a
    result, its preferable to put *EU* rather than *France* or *Cannes*.
-   When discussing national issues, as well as a tag describing the
    country it is useful to provide a descriptor highlighting the
    emphasis of the story. For example, a UK story describing a minister
    commenting on copyright would be tagged both "*UK*" and
    "*UKPolitics*", while a story on a decline in French music sales
    would be "*France*" and "*FranceEconomy*".
-   The location information should also bear in mind rotating Council
    Presidencies, including the emphasis and the position. So, for
    example a story on the European Independence arena which was held
    under the French Presidency would also include "*France*",
    "*FrancePolitics*" and "*FrancePresidency*" as well as any European
    descriptors.

**Key Actors**

-   Documenting the key actors within any story or link is key to
    finding relevant material in future research. Many stories mention
    side players but it is important to bear in mind the posterity
    intention of this system. An article going through lists of other
    companies and individuals mentioned merely in passion will create
    too much search noise if processed as additional tags.
-   Although key individuals are likely to be important it is best to
    restrict the information to positions. So rather than creating a
    "*CharlieMcCreevy*" tag, create a tag entitled
    "*EUCommissionInternalMarket*"
-   It is important to get the correct level of abstraction at the same
    time. For example, for a story regarding copyright it is necessary
    to include "*EUCommissionInternalMarket*" should include
    "*EUCommissionInternalMarket*" and "*EUCommission*", as people may
    look for copyright issues from a variety of EU Commission
    perspectives. In a similar way MySpace Music issues may not always
    be similar to MySpace issues. Therefore, either "*MySpace*" *and*
    "*MySpaceMusic*" gets used or just "*MySpace*", depending on the
    issues.
-   If the Actor concerned is a public affairs organisation then include
    the label "organisation" to highlight the information.

**Key Topic Points**

-   Gaining the most out of this area will take great care in the choice
    of topic descriptors. If unsure, it is best to search out previous
    examples to find the key terms to use.
-   If still unsure, include the tag "*TOPICFLAG*" to highlight to the
    designated staff member in charge of managing the system the
    problem. An RSS feed can be put into play so the staff member
    automatically receives an email (still to be done).
-   Getting the correct level of abstraction needs attention just like
    before. For example, a radio station would be classed as
    "*RadioStations*" and "*radio*".
-   The end topics should also contain key branch points. In the case of
    "*radio*" branches should also include such info as "*music*" or
    even "*Internet*" "*MP3s*" or "*streaming*", depending on the
    contexts the links contain.

**Initiatives and Issues**

-   Insert the tag "*initiatives*" to highlight that the link refers to
    a project of some sort, either envisaged or active, such as the
    ThreeStrikesRule.
-   Insert the tag "*issues*" to highlight an ongoing affair that
    effects the music industry, such as P2P, piracy, or antitrust.
-   The terms Public Affairs and Organisations have been used but their
    value is uncertain, its perhaps worth examination their benefit over
    time

**Rolling News, Single Stories, Information or Directive?**

-   This database contains a broad base of information types. It is
    useful to highlight this when writing tags so that people will be
    quickly able to hunt down information depending on their reasoning.
-   *Rolling News* is used to describe links where there is a collection
    of news stories (or more links) connected to a specific area. The
    rule of thumb is that there should be at least three relevant
    stories over the space of six month to be classed like this. Through
    reference if this rule is broken the tag should be removed by
    whoever notices this lapse in contributions.
-   *Single Stories* is used to describe link to a one off story that is
    worth reading in the future.
-   *Information* is used to describe a key point of information of some
    sort.
-   *Directive* is used to describe a key legislative text, such as an
    EU document. Also include the tag "information".

**Organisational Codes**

-   The tag \**RSS* references whether the link is included (or can be
    included) as part of the RSS feeder. The tag \**chk* means that the
    link does not provide a suitable RSS feed.
-   The tag \**2rl* signifies a link that needs exploring later for
    whatever reason
-   Although not started, a system for inserting signals as to who made
    a link or personal relevance could be useful, such as \**JT*, \**HS*
    and suchlike for quick reference and personalisation of the
    system.

**Journalism Codes**

-   The \# code indicates that the author (or at least publication in
    the right instance or news aggregator) has had their information
    recorded.
-   In the future, this can enable targeted mailouts that reach the most
    accurate audience possible rather than a homogeneous mass mailout,
    through combining topics with \# to generate a list of key
    contactable stakeholders.
-   Subsequent additions may include codes relating to specific mailouts
    and even responses. This can be used as a method to measuring
    success of campaigns.
-   For links which clearly relate to a specific journalist or writer
    within a news source, please include a \# with their name, so
    readers can quickly ascertain the originator. This should be
    especially employed for *Single News* pieces.

###Part Four: Managing Tags and Tag Bundles

4a) Clicking the <span class="inline-image">![Delicious user guide](https://bedtime.manufacturaindependente.org/img/delicious-user-guide-06.png)</span> on *Tag Options* on the blue bar on
the right-hand side (right) opens a list of options on tag viewing and
tag editing. To the familiar user editing tags information should not be
too complicated, as well as the viewing settings.

![Delicious user guide](https://bedtime.manufacturaindependente.org/imgdelicious-user-guide-13.png)

4b) Clicking on *manage tag bundles *enables the user to add new bundles
and add or remove new tags within bundles. The *tag bundles *form system
works in a similar way to before. Below are some tips on getting
effective use of the bundles.

![Delicious user guide](https://bedtime.manufacturaindependente.org/imgdelicious-user-guide-14.png)

**Abstraction**

-   Bundles are designed to carry all the key information points for
    people searching within a certain topic area. However, there is no
    need to provide all tags which relate to the topic area.
-   If the abstraction rule in the tags section is obeyed it should mean
    that it is unnecessary to include all topics, as *RadioStations* can
    be found through clicking on *radios*. This helps to both avoid
    noise when searching through areas but also to reinforce systematic
    searching approaches.

**Missing Tags**

-   When a new tag is created there is no guarantee that it will be
    found directly from people if they start looking through a bundle.
    It is necessary to systematically check to see if there are tags
    that need inserting into bundles, although the emergence of new tags
    will decline over time as greater areas of information are covered.
-   To search for unused tags click on *Unbundled Tags* on the
    right-hand sidebar, below the other tag bundle categories

**Inaccurately Placed Tags**

-   Although perhaps beneficial to check systematically for misplaced
    tags it is perhaps easier to weed out improper uses whilst people
    are working.

### Part Five: Expanding The Database

5a) To keep the database fresh it is useful to find people with similar
reading interests. The most beneficial approach is to find the readers
who have bookmarked a link prior to XXXXX.

![Delicious user guide](https://bedtime.manufacturaindependente.org/imgdelicious-user-guide-15.png)

Click on the tag *Single Stories*. These are the key stories within our
remit, so they are the perfect vehicle for expanding our knowledge pool.
Above is a typical news story. On the top right hand corner is the
number 4. Open the figure in a new tab.

![Delicious user guide](https://bedtime.manufacturaindependente.org/imgdelicious-user-guide-16.png)

5b) This will provide a list of the profiles (above) of relevant users.
Right click them and save their profiles to Delicious using the tag
\**del*. At a convenient time you should then spend time going through
their relevant tags. Make sure to check whether or not this user has
already been included within SquirrelTrumpet or not, as it would be a
duplication of effort. The quickest method is to search within
SquirrelTrumpet to see whether the users tag is in the *All Tags*
section.

When searching, attempt to find single tags which provide over 3
relevant stories to XXXXX within the space of 6 months. If this is the
case, save the bookmark like any other tag, including information on
content, source and the fact that it is Rolling News.

The user name will be included in the *Unbundled Tags* section.
Periodically please update them, placing them within the *News
Aggregator* section.

All Delicious tags are compatible as RSS feeds. They should be included
in the RSS feeder and then given the *\*rss* tag within Delicious. To
find news aggregators that have not been inputted into the RSS feeder
they should contain the *\*NA* tag but not *\*RSS*. If done
systematically, the difference should be apparent when searching
chronologically.
