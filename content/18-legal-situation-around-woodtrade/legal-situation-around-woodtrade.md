---
title: Legal situation around wood trade
contributor: An Mertens
type: text
pages: 1
---

 A collection of links that offer a start to read up on the legal situation around woodtrade.  
 “The EU Timber Regulation (EUTR) set out in 2013 to target companies importing timber and do something about the problem of illegal logging that rapidly destroys the world’s forests. In spite of noble intentions, the EUTR has created a lot of challenges within the European timber trade, because it’s hard to figure out exactly what it is you need to do to stay clear of trouble. (...)”  
 (Preferred by nature.org, continue reading in the first link)

 ---

- The EU page on it:  
<https://ec.europa.eu/environment/forests/index_en.htm>
- A 'translation' of the too complicated EU regulation:  
<https://preferredbynature.org/newsroom/460-timber-traders-across-europe-learned-how-dodge-risk-importing-illegal-timber>
- The regulation measures per country & their risk assessment - every country is given a quota!  
<https://preferredbynature.org/sourcinghub/timber>
- An activist organisation doing research on specific cases:  
<https://eia-international.org>
- A fact sheet of 2009:  
<https://wwfeu.awsassets.panda.org/downloads/wwf_foe_poll_factsheet.pdf>
- Forest Europe, a platform organisation organising conferences for the 47 ministers of EU countries (46+EU):  
<https://foresteurope.org/>
- A paper from 2002:  
<https://www.researchgate.net/publication/265197445_Deforestation_the_timber_trade_and_illegal_logging/link/5a0b17c1a6fdccc69ed9f9a7/download>
