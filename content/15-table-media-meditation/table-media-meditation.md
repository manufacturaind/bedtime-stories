---
title: Table Media Meditation
contributor: Francis Hunger
media: table-media-meditation.mp3
---

June 6, 2021

Listen to your breath. Breathe in. Breathe out. Once again: Breathe in.
Breathe out. Try not to control your breathing let the flow come
naturally as it happens. Now call an image to your inner eye. Call it
very slowly.

The image is an image of an empty table. You can think of your
spreadsheet application like Excel or Google Tables or Open Office
table. You may also think of the ancient table that is printed in a
book. To release the energy of the day, make sure you are thinking of an
*empty* table.

All your work today is done. You don't need to fill in anything. You
don't need to fill in data, you don't have to think about table
headings, you don't have to think about format, and you don't have to
think about operations like summarize or sort alphabetically. You have
before you in this relaxing table media meditation just an empty table.
It is the end of the day. You don't need to fill the table. It can
remain empty. You're just seeing a grid and the grid provides security,
stability, order, borders, and overview.

Listen to your breath while your inner eye gazes over the emptiness of
the table. Try to avoid thoughts of the tasks which should be recorded
in the table. Avoid thinking of the processes which you usually
formalize in such a table. Make sure the table in front of you stays
empty. All your work today is done. All your work today has been a
success. Try to get rid of negative emotions. Inside the table space
everything is positive. If you observe any negative emotions, send them
gracefully away, beyond the tables borders.

You can relax now. Listen to your breath. Evoke once again the image of
an empty table. Enjoy its emptiness, the quiet and the rigidness of the
grid. It provides security. Keep this security with you.

Thank you for being with me in this table media meditation. It ends now.
Go to sleep the day is over.
