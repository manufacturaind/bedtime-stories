---
title: Stacking as a strategy
contributor: Elodie Mugrefya
type: text
pages: 2
---

“Stacking as a strategy” is a statement my sister made as we were having a discussion on how the bureaucratic apparatus in Belgium impacts people who are considered as ‘foreigners.’ As a lawyer in Foreigners’ law, she sees the stacking as a strategy because it is a way of adding bureaucratic procedures onto bureaucratic procedures to people who are already in precarious situation. According to her, it is unclear where this tendency comes from. Whether it’s a fully conscious and intended practice from administrations and/or their workers or not, it does affect the ones having to face that system. It is interesting to see that procedures sometimes do not even follow the law; so the bureaucratic apparatus that is supposedly unmovable because of a stiffness inherited from rules and laws can sometimes bend under an undetermined pressure. After that discussion, I wrote a poem on this accumulation of strange uncountable matter that is sometimes inflexible and other times fluid. Here I’ve put this poem in discussion with my sister’s words from our discussion.

&nbsp; |
---------- |
<span class="text-center">Stacks, layers, strata; aggregation of matter.</span> <span class="text-center">Seemingly unmovable.</span> |
<span class="text-left">There used to be a provision in the general Law on the right to stay stating that foreigners’ children would inherit their parents’ rights.</span> |
<span class="text-center"> A special instrument might be necessary in order to perforate its deep crust.</span> <span class="text-center">From one angle.</span> |
<span class="text-left">Now, this provision is deleted from the law. Consequently, certain administrations have started to change their procedures.</span> |
<span class="text-center">Another instrument extracts the matter. </span> <span class="text-center">But if you find the good angle tho.</span> |
<span class="text-left">In certain administration, and this is apparently spreading, there’s no inheritance of rights anymore. Consequently a procedure of inheritance must be started</span> |
<span class="text-center">It renders up the stacking</span> <span class="text-center">Not especially close nor far</span> |
<span class="text-left">This is not an overtly complicated procedure but it’s an addition of procedures for people who are already dealing a lot with administrations from a very vulnerable position.</span> |
<span class="text-center">It's difficult to count anything.</span> <span class="text-center">Some things are moving.</span> |
<span class="text-left">Because of this legal grey area, there’s more space given for administrations to decide on how to proceed.</span> |
<span class="text-center">As the limits are not very clear.</span> <span class="text-center">Tiny elements whirling among hardness.</span> |
<span class="text-left">There is a clear tendency to complicate things for people who are ‘foreigners’ even if they are legally staying in the country.</span> |
<span class="text-center">It's dense and living</span> <span class="text-center">Slowly growing.</span> |
