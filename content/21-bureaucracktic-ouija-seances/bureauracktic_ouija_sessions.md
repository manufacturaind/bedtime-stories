---
title: Bureaucracktic ouija sessions
contributor: Simon Browne
type: text, image
pages: 2
---

Through the medium of a multi-user whiteboard drawing on Big Blue Button, we will conjure stories by drawing on characters that spell out our bureaucracktic concerns, desires and dreams. This will require a medium who asks questions, and spirits, who answer them by editing an image on the whiteboard.

---

(∩ᄑ_ᄑ)⊃━☆ﾟ*･｡*･:≡( ε:) simoon proposes: bureaucracktive ouija seances ☆’ﾟ･:*:･｡'★’ﾟ･:*:･｡: ﾉ(>ω<ﾉ)

15 min BBB sessions, held intermittently throughout Friday afternoon

Through the medium of a multi-user whiteboard drawing on Big Blue Button, we will conjure stories by drawing on characters that spell out our bureaucracktic concerns, desires and dreams. This will require a medium who asks questions, and spirits, who answer them by editing an image on the whiteboard.

[setting up the board: 5 mins]

* We join a breakout room/temporary room in BBB, which has an uploaded image that acts as our "divining background" (thanks @brendan)
* We decide who wants to ask questions (the medium), and who wants to answer them (spirits)

[séance: 10 mins]

* The medium asks spirits questions; these are best formulated ad lib to the spirits' responses
* Spirits use the interactive whiteboard features to respond (e.g. drawing/erasing/writing/hovering/moving etc) with microphone on mute
* The séance ends with the medium thanking spirits for their guidance

Transcription of séance held on December 12th, 2020:
I welcome any good spirits who are near to join our circle. Please make your presence known.
I'd like to ask you about some things that have been on my mind over the past week.

Where are the "cracks" in bureaucracksy?  
Are they thin lines, or deep divides?  
What falls into these cracks?  
What is forgotten?  
What is remembered?  

Do you fear bureaucracy?  
Do you embrace it?  
Does it bring you pleasure?  

Are you a number?  
If so, which one?  
What is the feeling of being a number?  
Do you feel it can go on forever?  
Where does the counting end?  

Do you have dreams, or nightmares about red tape?  
Do you dream of smooth, efficient systems and processes?  
What hope is there for bureaucracy?

Is there a style guide for bureaucracy?  
Is bureaucracy an aesthetic performance?  
Does it follow the latest trends, or is it in need of a makeover?  
What colours does it dress in?  
What does it wear on the weekend?  
Does it shop on the high street?  
Can I see the receipts?

Is bureaucracy a species?  
Can it evolve?  
Does it mutate?  
Does it have a life expectancy?  
What is the smallest unit of bureaucratic biology?  
Is it entropic, or negentropic?  

Can your experiences fit easily into a list, a grid, a table, a box?  
Does it help you, or those around you, to organise them in these structures?  
Are rules made just to be broken?  

I thank you spirits for coming to our circle and sharing your guidance.

![After the Bureaucracktic Ouija session](https://bedtime.manufacturaindependente.org/img/bureaucractic-ouija-after.png)
