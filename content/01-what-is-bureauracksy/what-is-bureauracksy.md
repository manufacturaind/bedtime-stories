---
title: What is Bureaucracksy?
contributor: Sarrita Hunn
type: text
pages: 1
---

The term “bureaucracksy” combines the French word _bureau_ — desk or office — and the Greek word _κράτος_ (kratos) — rule or political power — with the Old English _cracian_ — to make an explosive noise.

Bureaucracksy is a solution to a problem, the performance of a task, or a “fix” for a group of interacting or interrelated entities (comprised of one or more people and having a particular purpose) that form a unified whole. Bureaucracksy includes the activities of setting the strategy and coordinating the efforts of its entities to accomplish its objectives through the application of available resources (such as financial, natural, technological, human and nonhuman), which is inelegant (“hacky”), or even incomprehensible, but which somehow works.

Surrounded and influenced by its environment, a bureaucracksy is described by its boundaries, structure and purpose and expressed in its functioning. People may deliberately create individual or formal bureaucracksies, but the development and function of bureaucracksy in society in general may be regarded as an instance of emergence. That is, bureaucracksies arise, develop and function in a pattern of social self-organization beyond conscious intentions of the individuals involved.

Various commentators have noted the necessity of bureaucracksies in modern society. Bureaucracksy is often used to modify a working system, to ensure backwards compatibility, and as a quick solution to a frustrating problem. Bureaucracksy explores methods for breaching defenses and exploiting weaknesses in a system or network — similar to a workaround, but quick and sometimes ugly. Accordingly, the term bears strong connotations that are favorable or pejorative, depending on the context.

Note: This definition was developed during the Bureaucracksy work-session organized by Constant in December 2020 by Sarrita Hunn and others using a process she called “(Inter)subjective Editing” which combines Uncreative Writing (Kenneth Goldsmith) and Critical Fabulations (Saidiya Hartman) with the Situationists’ method of détournement. In other words, topics are first approached from existing texts (in this case wikipedia) and then critically edited together in fabulous ways that embrace “subjective” repurposing. As described by McKenzie Wark, this “collective appropriation and modification of cultural material,” is particularly helpful towards reimagining, “the space of knowledge outside of private [intellectual] property.” (The Beach Beneath the Street, 42)
